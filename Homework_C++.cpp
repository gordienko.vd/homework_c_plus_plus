﻿#include <iostream>

using namespace std;

class Animal {
public:
    virtual void voice() {
        cout << "Rrrrr"s << endl;
    }
    virtual ~Animal() = default;
};

class Cat : public Animal {
public:
    void voice() override {
        cout << "Myau"s << endl;
    }
};

class Dog : public Animal {
public:
    void voice() override {
        cout << "Woof"s << endl;
    }
};

class Cow : public Animal {
public:
    void voice() override {
        cout << "Mmmuuu"s << endl;
    }
};

int main() {

    Animal *animals[] = {new Cat(), new Dog(), new Cow()};

    for (auto &animal : animals) {
        animal->voice();
        delete animal;
    }

    auto **animals2 = new Animal*[3];
    animals2[0] = new Cat();
    animals2[1] = new Dog();
    animals2[2] = new Cow();

    for(int i = 0; i < 3; i++) {
        animals2[i]->voice();
        delete animals2[i];
    }
    delete[] animals2;


    Cat Murzik;
    Dog Sharik;
    Cow Murka;

    Animal* animals3[3] = {&Murzik, &Sharik, &Murka};

    Murzik = Cat();
    Sharik = Dog();
    Murka = Cow();

    for (auto &animal3 : animals3) {
        animal3->voice();
    }

    return 0;
}


